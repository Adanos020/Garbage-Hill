// Program modules.
#include <audio.hpp>
#include <resources.hpp>


// Audio's static members.
sf::Sound  Audio::sounds[10];
sf::Music* Audio::music;

// Audio system.
AudioSystem::AudioSystem()
{
        Subject::add(this);
}

void AudioSystem::on_notify(Message msg)
{
        switch (msg.id)
        {
                case MessageID::AUDIO_BETTER_MUSIC:
                        Audio::play_music("better music");
                        Audio::music->setPitch(1);
                        break;

                case MessageID::ENGINE_PUSH_MENU:
                        Audio::play_music("music");
                        break;

                case MessageID::ENGINE_PUSH_LEVEL:
                        Audio::music->setPitch(2);
                        Audio::play_sound("level start");
                        break;

                case MessageID::ENGINE_POP_STATE:
                        Audio::music->setPitch(0.5);
                        break;

                case MessageID::PLAYER_JUMPING:
                        Audio::play_sound("jump");
                        break;

                case MessageID::PLAYER_RING_COLLECTED:
                        Audio::play_sound("ring collect");
                        break;

                case MessageID::PLAYER_ENEMY_HIT:
                        Audio::play_sound("lost rings");
                        Audio::play_sound("damaged");
                        break;

                case MessageID::PLAYER_DIED:
                        Audio::stop_music();
                        Audio::play_music("failure");
                        break;

                case MessageID::PLAYER_WON:
                        Audio::stop_music();
                        Audio::play_sound("damaged");
                        Audio::play_music("victory");
                        break;

                default: break;
        }
}


// Audio.
Audio::~Audio()
{
        stop_music();
}

// Players.
void Audio::play_music(std::string id)
{
        music = Resources::song(id);
        music->stop();
        music->play();
}

void Audio::play_sound(std::string id)
{
        for (auto& sound : sounds)
        {
                if (sound.getStatus() != sf::Sound::Playing)
                {
                        sound.setBuffer(*Resources::sound(id));
                        sound.stop();
                        sound.play();
                        break;
                }
        }
}

void Audio::stop_music()
{
        music->stop();
}

void Audio::pause_music()
{
        music->pause();
}

void Audio::resume_music()
{
        music->play();
}

bool Audio::music_playing()
{
        return music->getStatus() == sf::Music::Playing;
}
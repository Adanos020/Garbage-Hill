// Program modules.
#include <lib/animated_sprite.hpp>


AnimatedSprite::AnimatedSprite() : sf::Sprite()
{
        curr_animation = NULL;
}

void AnimatedSprite::add_animation(std::string id, Animation animation)
{
        animations[id] = animation;
        if (animations.size() == 1)
        {
                play(id);
                stop();
        }
}

void AnimatedSprite::update(double dt)
{
        if (curr_animation != NULL)
        {
                curr_animation->update(dt);
                this->setTextureRect(curr_animation->current_frame());
        }
}

void AnimatedSprite::play(std::string id)
{
        curr_animation = &animations[id];
        this->setTexture(*curr_animation->sprite_sheet);
        curr_animation->play();
}

void AnimatedSprite::pause()
{
        if (curr_animation != NULL) curr_animation->pause();
}

void AnimatedSprite::resume()
{
        if (curr_animation != NULL) curr_animation->resume();
}

void AnimatedSprite::stop()
{
        if (curr_animation != NULL) curr_animation->stop();
}
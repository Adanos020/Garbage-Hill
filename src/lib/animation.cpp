// Program modules.
#include <lib/animation.hpp>


Animation::Animation(sf::Texture* sprite_sheet, double frame_time, bool looped,
                     std::vector<sf::IntRect> frames)
: curr_frame  (0)
, frame_time  (frame_time)
, loop        (looped)
, sprite_sheet(sprite_sheet)
, frames      (frames)
, plays       (false)
{
}


void Animation::add_frame(sf::IntRect frame)
{
        frames.push_back(frame);
}

void Animation::update(double dt)
{
        if (!plays) return;

        time_elapsed += dt;
        while (time_elapsed >= frame_time)
        {
                time_elapsed -= frame_time;
                ++curr_frame;
                if (curr_frame >= frames.size())
                {
                        if (loop) curr_frame = 0;
                        else    --curr_frame;
                }
        }
}

void Animation::play()
{
        curr_frame = 0;
        plays      = true;
}

void Animation::stop()
{
        curr_frame = 0;
        plays      = false;
}

void Animation::pause()
{
        plays = false;
}

void Animation::resume()
{
        plays = true;
}

bool Animation::playing() const
{
        return plays;
}

void Animation::frame_duration(double ft)
{
        frame_time = ft;
}

double Animation::frame_duration() const
{
        return frame_time;
}

void Animation::looped(bool looped)
{
        loop = looped;
}

bool Animation::looped() const
{
        return loop;
}

sf::IntRect Animation::current_frame() const
{
        return frames[curr_frame];
}
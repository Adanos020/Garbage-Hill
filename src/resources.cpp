// Program modules.
#include <resources.hpp>


// Static members.
std::unordered_map<std::string, sf::Font*>        Resources::fonts;
std::unordered_map<std::string, sf::Texture*>     Resources::textures;
std::unordered_map<std::string, sf::Music*>       Resources::songs;
std::unordered_map<std::string, sf::SoundBuffer*> Resources::sounds;


// Destructor.
Resources::~Resources()
{
        unload_all();
}

// Loaders.
void Resources::load_font(std::string id, std::string path)
{
        auto font = new sf::Font();
        font->loadFromFile(path);
        fonts[id] = font;
}

void Resources::load_texture(std::string id, std::string path)
{
        auto texture = new sf::Texture();
        texture->loadFromFile(path);
        textures[id] = texture;
}

void Resources::load_song(std::string id, std::string path, bool looped)
{
        auto song = new sf::Music();
        song->openFromFile(path);
        song->setLoop(looped);
        songs[id] = song;
}

void Resources::load_sound(std::string id, std::string path)
{
        auto sound = new sf::SoundBuffer();
        sound->loadFromFile(path);
        sounds[id] = sound;
}

// Unloaders.
void Resources::unload_font(std::string id)
{
        delete fonts[id];
        fonts.erase(id);
}

void Resources::unload_texture(std::string id)
{
        delete textures[id];
        textures.erase(id);
}

void Resources::unload_song(std::string id)
{
        delete songs[id];
        songs.erase(id);
}

void Resources::unload_sound(std::string id)
{
        delete sounds[id];
        sounds.erase(id);
}

void Resources::unload_fonts()
{
        fonts.clear();
}

void Resources::unload_textures()
{
        textures.clear();
}

void Resources::unload_songs()
{
        songs.clear();
}

void Resources::unload_sounds()
{
        sounds.clear();
}

void Resources::unload_all()
{
        unload_fonts();
        unload_textures();
        unload_songs();
        unload_sounds();
}

// Accessors.
sf::Font* Resources::font(std::string id)
{
        return fonts[id];
}

sf::Texture* Resources::texture(std::string id)
{
        return textures[id];
}

sf::Music* Resources::song(std::string id)
{
        return songs[id];
}

sf::SoundBuffer* Resources::sound(std::string id)
{
        return sounds[id];
}
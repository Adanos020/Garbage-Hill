// Program modules.
#include <engine.hpp>
#include <global.hpp>
#include <resources.hpp>
#include <state.hpp>

// SFML.
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

// STL.
#include <stack>

// STDLIB.
#include <cstdlib>


Engine::Engine(unsigned xres, unsigned yres)
{
        Global::resolution = sf::Vector2i(xres, yres);
        Global::floor_level = (float) yres * 360 / 497;

        Subject::add(this);

        load_resources();

        window.create(sf::VideoMode(xres, yres, 32), "Garbage Hill", sf::Style::Close);
        window.setFramerateLimit(60);
        
        Global::game_timer = 0;
        Subject::notify(MessageID::ENGINE_PUSH_MENU);
}

void Engine::run()
{
        sf::Clock clock;

        double       lag        = 0.0;
        const double frame_time = 1.0 / 60;

        while (window.isOpen())
        {
                handle_input();

                // Updating the current state.
                lag += clock.restart().asSeconds();
                while (lag >= frame_time)
                {
                        states.top()->update(frame_time);
                        Global::game_timer += frame_time;
                        lag -= frame_time;
                }

                draw();

                if (!Audio::music_playing())
                {
                        if (Global::failure || Global::victory)
                        {
                                Subject::notify(MessageID::ENGINE_POP_STATE);
                                Global::failure = false;
                                Global::victory = false;
                        }
                        Subject::notify(MessageID::AUDIO_BETTER_MUSIC);
                }
        }
}

void Engine::load_resources()
{
        // Fonts.
        Resources::load_font("comic sans", "assets/font/comic_sans.ttf");

        // Textures.
        Resources::load_texture("gotta go fast",   "assets/gfx/gotta_go_fast.png");
        Resources::load_texture("menu background", "assets/gfx/menu_bkg.png");
        Resources::load_texture("sanic",           "assets/gfx/sanic.png");
        Resources::load_texture("ring",            "assets/gfx/ring.png");
        Resources::load_texture("motobug",         "assets/gfx/motobug.png");
        Resources::load_texture("finisher",        "assets/gfx/finisher.png");
        
        // Music.
        Resources::load_song("music",        "assets/music/music.ogx");
        Resources::load_song("better music", "assets/music/better_music.ogg", false);
        Resources::load_song("failure",      "assets/music/failure.ogg",      false);
        Resources::load_song("victory",      "assets/music/victory.ogg",      false);
        
        // Sound effects.
        Resources::load_sound("jump",         "assets/sfx/S1_A0.wav");
        Resources::load_sound("level start",  "assets/sfx/S1_A8.wav");
        Resources::load_sound("ring collect", "assets/sfx/S1_B5.wav");
        Resources::load_sound("sanic boost",  "assets/sfx/S1_BE.wav");
        Resources::load_sound("lost rings",   "assets/sfx/S1_C6.wav");
        Resources::load_sound("damaged",      "assets/sfx/S1_CB.wav");
}

void Engine::handle_input()
{
        sf::Event event;
        while (window.pollEvent(event))
        {
                if (event.type == sf::Event::Closed)
                        window.close();

                states.top()->handle_input(event);
        }
}

void Engine::draw()
{
        window.clear();
        states.top()->draw(window);
        window.display();
}

void Engine::on_notify(Message msg)
{
        switch (msg.id)
        {
        case MessageID::ENGINE_POP_STATE:
                if (!states.empty())
                {
                        delete states.top();
                        states.pop();
                }
                break;

        case MessageID::ENGINE_PUSH_LEVEL:
                states.push(new LevelState());
                break;

        case MessageID::ENGINE_PUSH_MENU:
                states.push(new MenuState());
                break;

        case MessageID::ENGINE_TERMINATE:
                window.close();
                break;

        case MessageID::PLAYER_DIED:
                Subject::notify(MessageID::ENGINE_POP_STATE);
                states.push(new DeathState());
                break;

        default: break;
        }
}


// Main.
int main()
{
        srand(time(0));

        Engine engine(1280, 720);
        engine.run();

        return 0;
}
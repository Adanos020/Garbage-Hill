// SFML.
#include <global.hpp>


// Static members.
sf::Vector2i Global::resolution;
float        Global::floor_level;
double       Global::game_timer;
int          Global::player_rings = 0;
bool         Global::victory = false;
bool         Global::failure = false;
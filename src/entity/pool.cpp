// Program modules.
#include <global.hpp>
#include <entity/pool.hpp>
#include <entity/player.hpp>


EntityPool::EntityPool()
{
        // Clearing the array from garbage.
        for (size_t i = 0; i < MAX_ENTITIES; ++i)
        {
                entities[i] = NULL;
        }

        // Player is always the first entity.
        add(new Player());
}

EntityPool::~EntityPool()
{
        for (auto& entity : entities)
        {
                delete entity;
                entity = NULL;
        }
}

void EntityPool::add(Entity* entity)
{
        for (size_t i = 0; i < MAX_ENTITIES; ++i)
        {
                // Empty entry means no delet.
                if (entities[i] == NULL)
                {
                        entities[i] = entity;
                        break;
                }
                // Not active and not active means delet.
                else if (!entities[i]->active)
                {
                        delete entities[i];
                        entities[i] = entity;
                        break;
                }
        }
}

void EntityPool::update(double dt)
{
        // Updating the entities.
        for (size_t i = 0; i < MAX_ENTITIES; ++i)
        {
                if (entities[i] != NULL && entities[i]->active)
                {
                        entities[i]->update(dt);
                }
        }

        if (entities[0] == NULL || Global::victory) return;

        // Checking if player collides with any.
        for (size_t i = 1; i < MAX_ENTITIES; ++i)
        {
                if (entities[i] != NULL && entities[i]->active)
                {
                        entities[i]->collide(*dynamic_cast<Player*>(entities[0]));
                }
        }
}

void EntityPool::draw(sf::RenderTarget& target) const
{
        // Rendering all entities.
        for (size_t i = 0; i < MAX_ENTITIES; ++i)
        {
                if (entities[i] != NULL && entities[i]->active)
                        target.draw(*entities[i]);
        }
}

sf::Vector2f EntityPool::player_position() const
{
        return entities[0]->getPosition();
}
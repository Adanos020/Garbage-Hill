// Program modules.
#include <global.hpp>
#include <entity/finisher.hpp>
#include <entity/spawn.hpp>


Entity* FinisherSpawn::spawn()
{
        if (Global::game_timer >= spawn_time)
        {
                return new Finisher(position, velocity);
        }
        return NULL;
}
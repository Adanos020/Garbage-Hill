// Program modules.
#include <global.hpp>
#include <resources.hpp>
#include <lib/math.hpp>
#include <entity/player.hpp>

// STDLIB.
#include <cstdio>


Player::Player()
{
        Subject::add(this);

        state = PlayerState::WALKING;

        auto tex = Resources::texture("gotta go fast");

        this->active = true;
        this->setPosition(Global::resolution.x / 2, Global::resolution.y * 497 / 360);

        sprite.setTexture(*tex);
        sprite.setScale(0.1, 0.1);
}

Player::~Player()
{
        Subject::remove(this);
}

bool Player::immune() const
{
        return immune_time > 0;
}

void Player::on_notify(Message msg)
{
        switch (msg.id)
        {
                case MessageID::PLAYER_WALK_LEFT:
                        velocity.x = -vel_walking;
                        break;

                case MessageID::PLAYER_WALK_RIGHT:
                        velocity.x = vel_walking;
                        break;

                case MessageID::PLAYER_WALK_STOP:
                        velocity.x = 0;
                        break;

                case MessageID::PLAYER_JUMP_START:
                        if (state == PlayerState::WALKING)
                        {
                                Subject::notify(MessageID::PLAYER_JUMPING);
                                state = PlayerState::JUMPING;
                                velocity.y = -vel_jumping;
                        }
                        break;

                case MessageID::PLAYER_JUMP_STOP:
                        if (state == PlayerState::JUMPING)
                        {
                                state = PlayerState::FALLING;
                                velocity.y = 200;
                        }
                        break;

                case MessageID::PLAYER_ENEMY_HIT:
                        if (Global::player_rings > 0)
                        {
                                immune_time = max_immune_time;
                                sprite.setColor(sf::Color(255, 0, 0, 128));
                        }
                        else
                        {
                                Global::failure = true;
                                Subject::notify(MessageID::PLAYER_DIED);
                        }
                        break;

                default: break;
        }
}

void Player::update(double dt)
{
        if (immune_time != 0 && (immune_time -= dt) <= 0)
        {
                sprite.setColor(sf::Color::White);
                immune_time = clamp(immune_time, 0.0, max_immune_time);
        }

        if (state != PlayerState::WALKING)
        {
                velocity.y += 1000 * dt;
                velocity.y = clamp((double) velocity.y, -vel_jumping, vel_jumping);

                if (state == PlayerState::JUMPING)
                {
                        if (velocity.y == 0) state = PlayerState::FALLING;
                }
        }

        // Moving the sprite.
        this->move(velocity * (float) dt);
        this->setPosition(clamp(this->getPosition(), sf::Vector2f(0, 0),
                sf::Vector2f(Global::resolution.x - boundbox().width, Global::floor_level - boundbox().height)));

        // Stop falling when reached the ground.
        if (state == PlayerState::FALLING && this->getPosition().y == Global::floor_level - boundbox().height)
        {
                state = PlayerState::WALKING;
                velocity.y = 0;
        }
}

void Player::collide(Player&)
{
}

sf::FloatRect Player::boundbox() const
{
        auto bounds = sprite.getGlobalBounds();
        bounds.left    = this->getPosition().x + 20;
        bounds.top     = this->getPosition().y + 20;
        bounds.width  -= 40;
        bounds.height -= 40;
        return bounds;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
        states.transform *= getTransform();
        target.draw(sprite, states);
}

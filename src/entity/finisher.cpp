// Program modules.
#include <global.hpp>
#include <resources.hpp>
#include <entity/finisher.hpp>
#include <entity/player.hpp>


Finisher::Finisher(sf::Vector2f position, sf::Vector2f velocity)
{
        this->active = true;
        this->setPosition(position);
        this->velocity = velocity;

        Animation turning(Resources::texture("finisher"), 0.05, false);

        for (int i = 0; i < 7; ++i)
        {
                turning.add_frame(sf::IntRect(  0, 0, 48, 48));
                turning.add_frame(sf::IntRect( 48, 0, 48, 48));
                turning.add_frame(sf::IntRect( 96, 0, 48, 48));
                turning.add_frame(sf::IntRect(144, 0, 48, 48));
        }
        turning.add_frame(sf::IntRect(192, 0, 48, 48));
        turning.add_frame(sf::IntRect(240, 0, 48, 48));
        turning.add_frame(sf::IntRect(288, 0, 48, 48));
        for (int i = 0; i < 7; ++i)
        {
                turning.add_frame(sf::IntRect(336, 0, 48, 48));
                turning.add_frame(sf::IntRect(384, 0, 48, 48));
                turning.add_frame(sf::IntRect(432, 0, 48, 48));
                turning.add_frame(sf::IntRect(480, 0, 48, 48));
        }
        turning.add_frame(sf::IntRect(528, 0, 48, 48));
        turning.add_frame(sf::IntRect(576, 0, 48, 48));
        turning.add_frame(sf::IntRect(624, 0, 48, 48));
        turning.add_frame(sf::IntRect(672, 0, 48, 48));

        sprite.add_animation("turning", turning);
        sprite.play("turning");
        sprite.stop();
        sprite.setScale(5, 5);
}

void Finisher::update(double dt)
{
        sprite.update(dt);
        this->move(velocity * (float) dt);
}

void Finisher::collide(Player& player)
{
        if (this->boundbox().intersects(player.boundbox()))
        {
                sprite.play("turning");
                Global::victory = true;
                Subject::notify(MessageID::PLAYER_WON);
        }
}

sf::FloatRect Finisher::boundbox() const
{
        auto bounds = sprite.getGlobalBounds();
        bounds.left = this->getPosition().x;
        bounds.top  = this->getPosition().y;
        return bounds;
}

void Finisher::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
        states.transform *= getTransform();
        target.draw(sprite, states);
}
// Program modules.
#include <global.hpp>
#include <observer.hpp>
#include <resources.hpp>
#include <entity/motobug.hpp>
#include <entity/player.hpp>


Motobug::Motobug(sf::Vector2f pos, sf::Vector2f vel)
{
        this->active = true;
        this->setPosition(pos);
        this->velocity = vel;

        Animation floating(Resources::texture("motobug"), 0.1);
        floating.add_frame(sf::IntRect(  0, 0, 41, 21));
        floating.add_frame(sf::IntRect( 41, 0, 41, 21));
        floating.add_frame(sf::IntRect( 82, 0, 41, 21));
        floating.add_frame(sf::IntRect(123, 0, 41, 21));

        sprite.add_animation("floating", floating);
        sprite.play("floating");
        sprite.setScale(5, 5);
}

Motobug::Motobug(float posx, float posy, float velx, float vely)
        : Motobug(sf::Vector2f(posx, posy), sf::Vector2f(velx, vely))
{}

void Motobug::update(double dt)
{
        sprite.update(dt);

        this->move(velocity * (float) dt);
        if (this->getPosition().y < 0 || this->getPosition().y > Global::floor_level - boundbox().height) velocity.y = -velocity.y;

        // Deleting the ring if it's offscreen.
        if (this->getPosition().x < -boundbox().width) active = false;
}

void Motobug::collide(Player& player)
{
        if (!player.immune() && this->boundbox().intersects(player.boundbox()))
        {
                Subject::notify(MessageID::PLAYER_ENEMY_HIT);
                active = false;
        }
}

sf::FloatRect Motobug::boundbox() const
{
        auto bounds = sprite.getGlobalBounds();
        bounds.left = this->getPosition().x;
        bounds.top  = this->getPosition().y;
        return bounds;
}

void Motobug::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
        states.transform *= getTransform();
        target.draw(sprite, states);
}
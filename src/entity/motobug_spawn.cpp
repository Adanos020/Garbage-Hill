// Program modules.
#include <global.hpp>
#include <entity/motobug.hpp>
#include <entity/spawn.hpp>


Entity* MotobugSpawn::spawn()
{
        if (Global::game_timer >= spawn_time)
                return new Motobug(position, velocity);
        return NULL;
}
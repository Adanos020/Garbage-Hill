// Program modules.
#include <entity/ring.hpp>
#include <entity/spawn.hpp>
#include <global.hpp>


Entity* RingSpawn::spawn()
{
        if (Global::game_timer >= spawn_time)
                return new Ring(position, velocity);
        return NULL;
}
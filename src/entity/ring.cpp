// Program modules.
#include <observer.hpp>
#include <resources.hpp>
#include <entity/player.hpp>
#include <entity/ring.hpp>
#include <global.hpp>
#include <lib/math.hpp>


Ring::Ring(sf::Vector2f pos, sf::Vector2f vel)
{
        this->active = true;
        this->setPosition(pos);
        this->velocity = vel;

        Animation rotating(Resources::texture("ring"), 0.1);
        rotating.add_frame(sf::IntRect(  0, 0, 64, 64));
        rotating.add_frame(sf::IntRect( 64, 0, 64, 64));
        rotating.add_frame(sf::IntRect(128, 0, 64, 64));
        rotating.add_frame(sf::IntRect(192, 0, 64, 64));

        sprite.add_animation("rotating", rotating);
        sprite.play("rotating");
}

Ring::Ring(float posx, float posy, float velx, float vely)
        : Ring(sf::Vector2f(posx, posy), sf::Vector2f(velx, vely))
{}

void Ring::update(double dt)
{
        sprite.update(dt);

        // Moving the righ.
        this->move(velocity * (float) dt);
        if (this->getPosition().y < 0 || this->getPosition().y > Global::floor_level - boundbox().height) velocity.y = -velocity.y;

        // Keeping the ring on screen.
        this->setPosition(this->getPosition().x, clamp(this->getPosition().y, 0.f, Global::floor_level - boundbox().height));

        // Deleting the ring if it's offscreen.
        if (this->getPosition().x < -boundbox().width) active = false;
}

void Ring::collide(Player& player)
{
        if (!player.immune() && this->boundbox().intersects(player.boundbox()))
        {
                ++Global::player_rings;
                Subject::notify(MessageID::PLAYER_RING_COLLECTED);
                active = false;
        }
}

sf::FloatRect Ring::boundbox() const
{
        auto bounds = sprite.getGlobalBounds();
        bounds.left = this->getPosition().x;
        bounds.top  = this->getPosition().y;
        return bounds;
}

void Ring::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
        states.transform *= getTransform();
        target.draw(sprite, states);
}
// Program modules.
#include <entity/spawner.hpp>

// STDLIB.
#include <cstdio>
#include <cstring>


void Spawner::load(std::string path)
{
        FILE* file = fopen(path.c_str(), "r");

        while (!feof(file))
        {
                char  id[5];
                sf::Vector2f pos;
                sf::Vector2f vel;
                float stim;

                fscanf(file, "%s %f %f %f %f %f", id, &pos.x, &pos.y, &vel.x, &vel.y, &stim);

                if (std::string("ring") == id)
                {
                        auto rs = new RingSpawn();
                        rs->position = sf::Vector2f(pos.x, pos.y);
                        rs->velocity = sf::Vector2f(vel.x, vel.y);
                        rs->spawn_time = stim;

                        spawns.push(rs);
                }
                else if (std::string("mbug") == id)
                {
                        auto ms = new MotobugSpawn();
                        ms->position = sf::Vector2f(pos.x, pos.y);
                        ms->velocity = sf::Vector2f(vel.x, vel.y);
                        ms->spawn_time = stim;

                        spawns.push(ms);
                }
                else if (std::string("fnsh") == id)
                {
                        auto fs = new FinisherSpawn();
                        fs->position = sf::Vector2f(pos.x, pos.y);
                        fs->velocity = sf::Vector2f(vel.x, vel.y);
                        fs->spawn_time = stim;

                        spawns.push(fs);
                }
        }

        fclose(file);
}

Entity* Spawner::spawn()
{
        if (spawns.empty()) return NULL;

        auto entity = spawns.front()->spawn();
        if (entity != NULL) spawns.pop();

        return entity;
}
// Program modules.
#include <observer.hpp>


// Static members.
std::vector<Observer*> Subject::observers;
std::vector<bool>      Subject::deleted;


void Subject::add(Observer* observer)
{
        for (size_t i = 0; i < observers.size(); ++i)
        {
                if (deleted[i])
                {
                        observers[i] = observer;
                        deleted[i] = false;
                        return;
                }
        }

        observers.push_back(observer);
        deleted.push_back(false);
}

void Subject::remove(Observer* observer)
{
        for (size_t i = 0; i < observers.size(); ++i)
        {
                if (observer == observers[i]) deleted[i] = true;
        }
}

void Subject::notify(Message msg)
{
        for (size_t i = 0; i < observers.size(); ++i)
        {
                if (!deleted[i]) observers[i]->on_notify(msg);
        }
}

void Subject::notify(MessageID id, std::string data)
{
        notify(Message{id, data});
}
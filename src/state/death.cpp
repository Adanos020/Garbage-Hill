// Program modules.
#include <audio.hpp>
#include <global.hpp>
#include <lib/math.hpp>
#include <observer.hpp>
#include <resources.hpp>
#include <state.hpp>


DeathState::DeathState()
{
        auto bkg = Resources::texture("menu background");
        bkg->setRepeated(true);

        background.setPosition(0, 0);
        background.setSize(sf::Vector2f(fit(sf::Vector2i(bkg->getSize()), Global::resolution)));
        background.setTexture(bkg);

        game_over.setFont(*Resources::font("comic sans"));
        game_over.setCharacterSize(100);
        game_over.setFillColor(sf::Color::Black);
        game_over.setOutlineColor(sf::Color::Red);
        game_over.setOutlineThickness(10);
        game_over.setString("game Over");
        game_over.setPosition(sf::Vector2f(Global::resolution) / 2.f);
}

void DeathState::update(double dt)
{
        if (!Audio::music_playing()) Subject::notify(MessageID::ENGINE_POP_STATE);
}

void DeathState::draw(sf::RenderTarget& target)
{
        target.draw(background);
        target.draw(game_over);
}

void DeathState::handle_input(sf::Event& event)
{
        if (event.type == sf::Event::KeyPressed)
        {
                switch (event.key.code)
                {
                        case sf::Keyboard::Escape:
                                Subject::notify(MessageID::ENGINE_POP_STATE);
                                break;

                        default: break;
                }
        }
}
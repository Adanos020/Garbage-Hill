// Program modules.
#include <audio.hpp>
#include <entity/player.hpp>
#include <entity/ring.hpp>
#include <global.hpp>
#include <lib/math.hpp>
#include <observer.hpp>
#include <resources.hpp>
#include <state.hpp>


LevelState::LevelState()
{
        Subject::add(this);

        bkg_speed = 1500;

        auto bkg = Resources::texture("menu background");
        bkg->setRepeated(true);

        background.setPosition(0, 0);
        background.setSize((sf::Vector2f) fit((sf::Vector2i) bkg->getSize(), Global::resolution));
        background.setTexture(bkg);

        rings.setFont(*Resources::font("comic sans"));
        rings.setCharacterSize(50);
        rings.setFillColor(sf::Color::Yellow);
        rings.setOutlineColor(sf::Color::Red);
        rings.setOutlineThickness(10);
        rings.setString("rings: 0");
        rings.setPosition(50, Global::resolution.y - 100);

        act_passed.setFont(*Resources::font("comic sans"));
        act_passed.setCharacterSize(100);
        act_passed.setFillColor(sf::Color::Blue);
        act_passed.setOutlineColor(sf::Color::Red);
        act_passed.setOutlineThickness(10);
        act_passed.setString("act pASSed");
        act_passed.setPosition(sf::Vector2f(Global::resolution) / 2.f);

        spawner.load("assets/level");

        Global::game_timer = 0;
}

LevelState::~LevelState()
{
        Subject::remove(this);
}

void LevelState::update(double dt)
{
        // Spawning new entities.
        while (true)
        {
                auto entity = spawner.spawn();
                if (entity != NULL) entities.add(entity);
                else                break;
        }

        // Scrolling the background.
        auto frame = background.getTextureRect();
        frame.left = (int) (frame.left + bkg_speed * dt) % frame.width; // Preventing from integer overflow.
        background.setTextureRect(frame);

        // Updating current entities.
        entities.update(dt);
}

void LevelState::draw(sf::RenderTarget& target)
{
        target.draw(background);
        target.draw(rings);
        if (Global::victory) target.draw(act_passed);
        entities.draw(target);
}

void LevelState::handle_input(sf::Event& event)
{
        if (event.type == sf::Event::KeyPressed)
        {
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                        if (!Global::victory) Subject::notify(MessageID::ENGINE_POP_STATE);
                        break;

                case sf::Keyboard::Left:
                        Subject::notify(MessageID::PLAYER_WALK_LEFT);
                        break;

                case sf::Keyboard::Right:
                        Subject::notify(MessageID::PLAYER_WALK_RIGHT);
                        break;

                case sf::Keyboard::Space:
                        Subject::notify(MessageID::PLAYER_JUMP_START);
                        break;

                default: break;
                }
        }
        else if (event.type == sf::Event::KeyReleased)
        {
                if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::Right)
                {
                        /**/ if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                                Subject::notify(MessageID::PLAYER_WALK_LEFT);
                        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                                Subject::notify(MessageID::PLAYER_WALK_RIGHT);
                        else
                                Subject::notify(MessageID::PLAYER_WALK_STOP);
                }
                else if (event.key.code == sf::Keyboard::Space)
                {
                        Subject::notify(MessageID::PLAYER_JUMP_STOP);
                }
        }
}

void LevelState::on_notify(Message msg)
{
        switch (msg.id)
        {
        case MessageID::PLAYER_ENEMY_HIT:
                for (; Global::player_rings > 0; --Global::player_rings)
                {
                        entities.add(new Ring(entities.player_position(), sf::Vector2f((rand() % 300 + 300) * (rand() % 2 ? 1 : -1),
                                                                                       (rand() % 300 + 300) * (rand() % 2 ? 1 : -1))));
                }

        case MessageID::PLAYER_RING_COLLECTED:
                rings.setString(std::string("rings: ") + std::to_string(Global::player_rings));
                break;

        case MessageID::PLAYER_WON:
                Global::victory = true;
                bkg_speed = 0;
                break;
        
        default: break;
        }
}

// Program modules.
#include <global.hpp>
#include <lib/math.hpp>
#include <observer.hpp>
#include <resources.hpp>
#include <state.hpp>

// SFML.
#include <SFML/Graphics/Text.hpp>

// STDLIB.
#include <cstdlib>


MenuState::MenuState()
{
        auto bkg = Resources::texture("menu background");
        bkg->setRepeated(true);

        background.setPosition(0, 0);
        background.setSize((sf::Vector2f) fit((sf::Vector2i) bkg->getSize(), Global::resolution));
        background.setTexture(bkg);

        sanic.setTexture(*Resources::texture("sanic"));
        sanic_xspeed = rand() % 500  + 100;
        sanic_yspeed = rand() % 500  + 100;
        sanic_rspeed = rand() % 1000 - 500;

        title.setPosition(-10, 50);
        title.setFont(*Resources::font("comic sans"));
        title.setString("SANIC DA HEGEHOGE\ngARBAGE hILL");
        title.setCharacterSize(120);
        title.setFillColor(sf::Color::Red);
        title.setOutlineColor(sf::Color::Green);
        title.setOutlineThickness(10);

        author.setFont(*Resources::font("comic sans"));
        author.setString("bY aDam gąsior");
        author.setCharacterSize(50);
        author.setFillColor(sf::Color::Green);
        author.setOutlineColor(sf::Color::Black);
        author.setOutlineThickness(10);
        author.setPosition(Global::resolution.x - author.getGlobalBounds().width + 20, 50);

        controls.setPosition(60, 60);
        controls.setFont(*Resources::font("comic sans"));
        controls.setString("controls:\nenter-play the game\neSC-exit\narrowS-move\nSPACe-jump");
        controls.setCharacterSize(60);
        controls.setFillColor(sf::Color::Red);
        controls.setOutlineColor(sf::Color::Blue);
        controls.setOutlineThickness(5);
}

void MenuState::update(double dt)
{
        // Scrolling the background.
        auto frame = background.getTextureRect();
        frame.left = (int) (frame.left + bkg_speed * dt) % frame.width; // Preventing from integer overflow.
        background.setTextureRect(frame);

        // Moving sanic.
        sanic.rotate(sanic_rspeed * dt);
        sanic.move(sanic_xspeed * dt, sanic_yspeed * dt);

        if (sanic.getPosition().x < 0 || sanic.getPosition().x > Global::resolution.x) { sanic_xspeed = -sanic_xspeed; sanic_rspeed = rand() % 1000 - 500; }
        if (sanic.getPosition().y < 0 || sanic.getPosition().y > Global::resolution.y) { sanic_yspeed = -sanic_yspeed; sanic_rspeed = rand() % 1000 - 500; }
}

void MenuState::draw(sf::RenderTarget& target)
{
        target.draw(background);
        target.draw(title);
        target.draw(author);
        target.draw(controls);
        target.draw(sanic);
}

void MenuState::handle_input(sf::Event& event)
{
        if (event.type == sf::Event::KeyPressed)
        {
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                        Subject::notify(MessageID::ENGINE_TERMINATE);
                        break;

                case sf::Keyboard::Return:
                        Subject::notify(MessageID::ENGINE_PUSH_LEVEL);
                        break;

                default: break;
                }
        }
}
#pragma once


// Program modules.
#include <entity.hpp>
#include <lib/animated_sprite.hpp>

// SFML.
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>


class Motobug : public Entity
{
private:
        AnimatedSprite sprite;
        sf::Vector2f   velocity;

public:
        /** \brief Constructor.
         */
        Motobug(sf::Vector2f pos, sf::Vector2f vel);

        /** \brief Constructor.
         */
        Motobug(float posx, float posy, float velx, float vely);

        virtual void update(double dt) override;
        virtual void collide(Player&) override;
        virtual sf::FloatRect boundbox() const override;
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;
};
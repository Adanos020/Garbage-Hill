#pragma once


// Program modules.
#include <entity.hpp>

// SFML.
#include <SFML/System/Vector2.hpp>


class Spawn
{
protected:
        sf::Vector2f position;
        sf::Vector2f velocity;
        double       spawn_time; // Point in time in which the entity is gonna be spawned.

public:
        virtual Entity* spawn() = 0;
};

class RingSpawn : public Spawn
{
        friend class Spawner;

public:
        virtual Entity* spawn() override;
};


class MotobugSpawn : public Spawn
{
        friend class Spawner;

public:
        virtual Entity* spawn() override;
};


class FinisherSpawn : public Spawn
{
        friend class Spawner;

public:
        virtual Entity* spawn() override;
};
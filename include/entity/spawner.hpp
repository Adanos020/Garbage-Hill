#pragma once


// Program modules.
#include <entity.hpp>
#include <entity/spawn.hpp>

// STL.
#include <queue>
#include <string>


class Spawner
{
private:
        std::queue<Spawn*> spawns;

public:
        /** \brief Loads the spawns data from a text file.
         */
        void load(std::string path);

        /** \return Next entity whose time to be spawned already came.
         */
        Entity* spawn();
};
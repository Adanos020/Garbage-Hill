#pragma once


// Program modules.
#include <lib/animated_sprite.hpp>
#include <entity.hpp>


class Player;

class Finisher : public Entity
{
private:
        AnimatedSprite sprite;
        sf::Vector2f   velocity;

public:
        /** \brief Constructor.
         */
        Finisher(sf::Vector2f position, sf::Vector2f velocity);

        virtual void update(double dt) override;
        virtual void collide(Player&) override;
        virtual sf::FloatRect boundbox() const override;
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;
};
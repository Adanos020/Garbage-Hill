#pragma once


// Program modules.
#include <entity.hpp>

// SFML.
#include <SFML/Graphics/RenderTarget.hpp>


#define MAX_ENTITIES 50


class EntityPool
{
private:
        Entity* entities[MAX_ENTITIES];

public:
        /** \brief Constructor.
         */
        EntityPool();

        /** \brief Destructor.
         */
        ~EntityPool();

        /** \brief Adds a new entity to the pool.
         *  
         *  \param entity The entity to add.
         */
        void add(Entity* entity);

        /** \brief Updates the state of all active entities.
         *  
         *  \param dt Time step in seconds.
         */
        void update(double dt);

        /** \brief Draws all active entities to the screen.
         *  
         *  \param target Render target to which the entity will be drawn.
         */
        void draw(sf::RenderTarget& target) const;

        /** \returns Player's current position.
         */
        sf::Vector2f player_position() const;
};
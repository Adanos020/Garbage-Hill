#pragma once


// Program modules.
#include <observer.hpp>
#include <entity.hpp>

// SFML.
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>


enum class PlayerState
{
        WALKING, JUMPING, FALLING
};

/** \brief Player implementation of Entity.
 */
class Player : public Entity, public Observer
{
private:
        PlayerState state;

        sf::Sprite   sprite;
        sf::Vector2f velocity;

        const double vel_jumping = 800.0;
        const double vel_walking = 400.0;

        const double max_immune_time = 1.5;
        double       immune_time     = 0;

private:
        virtual void on_notify(Message msg) override;

public:
        /** \brief Constructor.
         */
        Player();

        /** \brief Destructor.
         */
        ~Player();

        bool immune() const;

        virtual void update(double dt) override;
        virtual void collide(Player&) override;
        virtual sf::FloatRect boundbox() const override;
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;
};

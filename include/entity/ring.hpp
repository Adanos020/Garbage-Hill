#pragma once


// Program modules.
#include <lib/animated_sprite.hpp>
#include <entity.hpp>

// SFML.
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>


/** Ring implementation of entity.
 */
class Ring : public Entity
{
private:
        AnimatedSprite sprite;
        sf::Vector2f   velocity;

public:
        /** \brief Constructor.
         */
        Ring(sf::Vector2f pos, sf::Vector2f vel);

        /** \brief Constructor.
         */
        Ring(float posx, float posy, float velx, float vely);

        virtual void update(double dt) override;
        virtual void collide(Player&) override;
        virtual sf::FloatRect boundbox() const override;
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;
};
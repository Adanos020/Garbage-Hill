#pragma once


// Program modules.
#include <observer.hpp>

// SFML.
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>


class Audio
{
        friend class AudioSystem;

private: // Disable constructor.
        Audio();

private:
        static sf::Sound  sounds[10];
        static sf::Music* music;

public:
        ~Audio();
        
        /** \brief Plays music.
         *  
         *  \param id Identifier of the song.
         */
        static void play_music(std::string id);

        /** \brief Plays a sound.
         *  
         *  \param id Identifier of the sound.
         */
        static void play_sound(std::string id);

        /** \brief Stops the currently playing song.
         */
        static void stop_music();

        /** \brief Pauses the currently playing song.
         */
        static void pause_music();

        /** \brief Resumes the currently playing song.
         */
        static void resume_music();

        /** \returns True if the music is playing, false otherwise.
         */
        static bool music_playing();
};

class AudioSystem : public Observer
{
public:
        /** Constructor.
         */
        AudioSystem();
        
private:
        virtual void on_notify(Message msg) override;
};
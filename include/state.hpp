#pragma once

// Program modules.
#include <entity/pool.hpp>
#include <entity/spawner.hpp>
#include <observer.hpp>

// SFML.
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>


/** Interface for game engine states.
 */
class GameState
{
public:
        virtual ~GameState() {}

        /** \brief Updates the state by a time period of one frame.
         * 
         *  \param dt The duration of a frame.
         */
        virtual void update(double dt) = 0;

        /** \brief Draws the contents of the state to a given render target.
         * 
         *  \target The target to which contents of the state are drawn.
         */
        virtual void draw(sf::RenderTarget& target) = 0;

        /** \brief Draws the contents of the state to a given render target.
         * 
         *  \target The target to which contents of the state are drawn.
         */
        virtual void handle_input(sf::Event& event) = 0;
};

/** Menu implementation of a game engine state.
 */
class MenuState : public GameState
{
private:
        sf::RectangleShape background;
        sf::Sprite         sanic;
        sf::Text           title;
        sf::Text           author;
        sf::Text           controls;

        const double bkg_speed = 200;

        double sanic_xspeed;
        double sanic_yspeed;
        double sanic_rspeed;

public:
        /** \brief Constructor.
         */
        MenuState();

        virtual void update(double dt) override;
        virtual void draw(sf::RenderTarget& target) override;
        virtual void handle_input(sf::Event& event) override;
};

/** Level implementation of a game engine state.
 */
class LevelState : public GameState, public Observer
{
private:
        sf::RectangleShape background;
        sf::Text           rings;
        sf::Text           act_passed;
        
        double bkg_speed;
        
        EntityPool entities;
        Spawner    spawner;

public:
        /** \brief Constructor.
         */
        LevelState();
        ~LevelState();

        virtual void update(double dt) override;
        virtual void draw(sf::RenderTarget& target) override;
        virtual void handle_input(sf::Event& event) override;
        virtual void on_notify(Message msg) override;
};

/** Level implementation of a game engine state.
 */
class DeathState : public GameState
{
private:
        sf::RectangleShape background;
        sf::Text           game_over;

public:
        /** \brief Constructor.
         */
        DeathState();

        virtual void update(double dt) override;
        virtual void draw(sf::RenderTarget& target) override;
        virtual void handle_input(sf::Event& event) override;
};
#pragma once


// SFML.
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>


class Player;

/** \brief Interface for an entity.
 */
class Entity : public sf::Drawable, public sf::Transformable
{
        friend class EntityPool;

protected:
        bool active;

public:
        /** \brief Updates the entity's state.
         *  
         *  \param dt The time step in seconds.
         */
        virtual void update(double dt) = 0;

        /** \brief Detects and reports collision with player.
         */
        virtual void collide(Player&) = 0;

        /** \return Bounding box of the entity.
         */
        virtual sf::FloatRect boundbox() const = 0;
};
#pragma once


// STDLIB.
#include <string>

// STL.
#include <vector>


/** \brief Identifier of a message.
 */
enum class MessageID
{
        AUDIO_BETTER_MUSIC,

        ENGINE_POP_STATE,
        ENGINE_PUSH_LEVEL,
        ENGINE_PUSH_MENU,
        ENGINE_TERMINATE,

        PLAYER_JUMP_START, // These two are just telling the observers that the jump button was pressed
        PLAYER_JUMP_STOP,  // or released.
        PLAYER_JUMPING,    // This one is the info that the player actually jumped, not that the jump button is pressed.
        PLAYER_WALK_LEFT,
        PLAYER_WALK_RIGHT,
        PLAYER_WALK_STOP,
        PLAYER_RING_COLLECTED,
        PLAYER_ENEMY_HIT,
        PLAYER_DIED,
        PLAYER_WON,
};


/** \brief Message structure.
 */
struct Message
{
        MessageID   id;
        std::string data;
};


/** \brief Observer interface.
 */
class Observer
{
        friend struct Subject;

public:
        /** Virtual destructor.
         */
        virtual ~Observer() {}

private:
        /** \brief Reacts to the message that was broadcasted.
         * 
         *  \param msg The message to react to.
         */
        virtual void on_notify(Message msg) = 0;
};


/** \brief Subject singleton.
 * 
 *  Subject is a singleton that has a collection of all Observers - objects that are
 *  listening to messages about important events in the game.
 */
struct Subject
{
private:
        static std::vector<Observer*> observers;
        static std::vector<bool>      deleted;

private: // Disabling the constructor.
        Subject() {}

public:
        /** \brief Adds a new Observer to the list.
         * 
         *  \param observer The observer that will be added.
         */
        static void add(Observer* observer);

        /** \brief Removes a new Observer from the list.
         * 
         *  \param observer The observer that will be removed.
         */
        static void remove(Observer* observer);

        /** \brief Broadcasts the message to all observers in the list.
         * 
         *  \param msg The message to broadcast.
         */
        static void notify(Message msg);

        /** \brief Broadcasts the message to all observers in the list.
         * 
         *  \param id Identifier of the message to broadcast.
         *  \param id Additional data carried with the message.
         */
        static void notify(MessageID id, std::string data = "");
};

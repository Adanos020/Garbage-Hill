#pragma once


// Program modules.
#include <lib/animation.hpp>

// SFML.
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>

// STL.
#include <unordered_map>


/** Extension of SFML's Sprite class for handling animations.
 */
class AnimatedSprite : public sf::Sprite
{
private:
        std::unordered_map<std::string, Animation> animations;
        
        Animation* curr_animation;

public: /** \brief Constructor.
         */
        AnimatedSprite();

        /** \brief Adds a new animation
         *  
         *  \param id        Name of the animation.
         *  \param animation The animation to add.
         */
        void add_animation(std::string id, Animation animation);

        /** \brief Updates the sprite's animation.
         * 
         *  \param dt Time step in seconds.
         */
        void update(double dt);

        /** \brief Plays an animation.
         * 
         *  \param id Name of the animation.
         */
        void play(std::string id);

        /** \brief Pauses the current animation.
         */
        void pause();

        /** \brief Resumes the current animation.
         */
        void resume();

        /** \brief Stops the current animation.
         */
        void stop();
};
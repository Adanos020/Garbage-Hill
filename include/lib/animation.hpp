#pragma once


// SFML.
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Rect.hpp>

// STL.
#include <vector>


/** \brief Animation data.
 */
struct Animation
{
        friend class AnimatedSprite;
private:
        std::vector<sf::IntRect> frames;

        sf::Texture* sprite_sheet;
                
        size_t curr_frame;
        double frame_time;
        double time_elapsed;
        bool   loop;
        bool   plays;

public:
        /** \brief Constructor.
         */
        Animation(sf::Texture* sprite_sheet = NULL, double frame_time = 0.2, bool looped = true,
                  std::vector<sf::IntRect> frames = std::vector<sf::IntRect>());

        /** \brief Adds a frame to the animation.
         */
        void add_frame(sf::IntRect frame);

        /** \brief Updates the animation.
         *  
         *  \param dt Time step in seconds.
         */
        void update(double dt);

        void play();
        void stop();
        void pause();
        void resume();
        bool playing() const;

        void frame_duration(double ft);
        double frame_duration() const;

        void looped(bool looped);
        bool looped() const;

        sf::IntRect current_frame() const;
};
#pragma once


// SFML.
#include <SFML/System/Vector2.hpp>

// STDLIB.
#include <algorithm>


template<typename T>
inline T clamp(T val,
               T min,
               T max)
{
        return std::min(std::max(val, min), max);
}

template<typename T>
inline sf::Vector2<T> clamp(sf::Vector2<T> val,
                            sf::Vector2<T> min,
                            sf::Vector2<T> max)
{
        val.x = clamp(val.x, min.x, max.x);
        val.y = clamp(val.y, min.y, max.y);
        return val;
}

template<typename T>
inline sf::Vector2<T> fit(sf::Vector2<T> val, sf::Vector2<T> in)
{
        if (val.x >= val.y)
        {
                double ratio = (double) in.y / val.y;
                return sf::Vector2<T>(val.x * ratio, val.y * ratio);
        }
        double ratio = (double) in.x / val.x;
        return sf::Vector2<T>(val.x * ratio, val.y * ratio);
}
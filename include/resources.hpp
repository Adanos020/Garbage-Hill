#pragma once


// SFML.
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>

// STL.
#include <unordered_map>


/** Resource manager.
 */
struct Resources
{
private: // Disabling the constructor.
        Resources();

private:
        static std::unordered_map<std::string, sf::Font*>        fonts;
        static std::unordered_map<std::string, sf::Texture*>     textures;
        static std::unordered_map<std::string, sf::Music*>       songs;
        static std::unordered_map<std::string, sf::SoundBuffer*> sounds;

public:
        ~Resources();

        /** \brief Loads a font from a file.
         *  
         *  \param id   Identifier of the new font.
         *  \param path Path to the file.
         */
        static void load_font(std::string id, std::string path);

        /** \brief Loads a texture from a file.
         *  
         *  \param id   Identifier of the new texture.
         *  \param path Path to the file.
         */
        static void load_texture(std::string id, std::string path);

        /** \brief Loads a song from a file.
         *  
         *  \param id   Identifier of the new song.
         *  \param path Path to the file.
         */
        static void load_song(std::string id, std::string path, bool looped = true);

        /** \brief Loads a sound from a file.
         *  
         *  \param id   Identifier of the new sound.
         *  \param path Path to the file.
         */
        static void load_sound(std::string id, std::string path);

        /** \brief Unloads a font.
         *  
         *  \param id Identifier of the font to unload.
         */
        static void unload_font(std::string id);

        /** \brief Unloads a texture.
         *  
         *  \param id Identifier of the texture to unload.
         */
        static void unload_texture(std::string id);

        /** \brief Unloads a song.
         *  
         *  \param id Identifier of the song to unload.
         */
        static void unload_song(std::string id);

        /** \brief Unloads a sound.
         *  
         *  \param id Identifier of the sound to unload.
         */
        static void unload_sound(std::string id);

        /** \brief Unloads all fonts.
         */
        static void unload_fonts();

        /** \brief Unloads all textures.
         */
        static void unload_textures();

        /** \brief Unloads all songs.
         */
        static void unload_songs();

        /** \brief Unloads all sounds.
         */
        static void unload_sounds();

        /** \brief Unloads all resources.
         */
        static void unload_all();

        /** \param id Identifier of desired font.
         *  
         *  \return The font of given id. 
         */
        static sf::Font* font(std::string id);

        /** \param id Identifier of desired texture.
         *  
         *  \return The texture of given id.
         */
        static sf::Texture* texture(std::string id);

        /** \param id Identifier of desired song.
         *  
         *  \return The song of given id.
         */
        static sf::Music* song(std::string id);

        /** \param id Identifier of desired sound.
         *  
         *  \return The sound of given id.
         */
        static sf::SoundBuffer* sound(std::string id);
};
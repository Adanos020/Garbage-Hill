#pragma once

// Program modules.
#include <audio.hpp>
#include <observer.hpp>
#include <state.hpp>

// SFML.
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

// STL.
#include <stack>


/** \brief Global state.
 */
class Engine : public Observer
{
private:
        AudioSystem            audio;
        std::stack<GameState*> states;
        sf::RenderWindow       window;

public:
        /** Constructor.
         */
        Engine(unsigned xres, unsigned yres);

        /** \brief Runs the engine.
         */
        void run();

private:
        void load_resources();
        void handle_input();
        void draw();

        virtual void on_notify(Message msg) override;
};
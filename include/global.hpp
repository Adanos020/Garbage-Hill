#pragma once


// SFML.
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>


struct Global
{
private: // Disabling the constructor.
        Global();

public:
        static sf::Vector2i resolution;
        static double       game_timer;
        static float        floor_level;
        static int          player_rings;
        static bool         failure;
        static bool         victory;
};